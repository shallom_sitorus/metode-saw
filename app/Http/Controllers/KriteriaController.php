<?php

namespace App\Http\Controllers;

use App\Models\Crips;
use App\Models\Kriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KriteriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['kriteria'] = Kriteria::orderBy('nama_kriteria', 'ASC')->get();
        return view('admin.kriteria.index', $data);
    }

    public function store(Request $request)
    {
        // dd($request);
        try{
            DB::beginTransaction();
            $validator = Validator::make($request->all(),[
                'nama_kriteria' => 'required|string',
                'attribut'      => 'required|string',
                'bobot'         => 'required|numeric',
            ]);
    
            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
    
            Kriteria::create([
                'nama_kriteria' => request()->nama_kriteria,
                'attribut' => request()->attribut,
                'bobot' => request()->bobot,
            ]);

            DB::commit();
            return redirect('kriteria')->with('success', 'Kriteria berhasil ditambahkan!');
        }catch(\Exception $e){
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
        
    }
    public function edit($id)
    {
        $data['kriteria'] = Kriteria::findOrFail($id);
        return view('admin.kriteria.edit', $data);
    }

    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $kriteria = Kriteria::findOrFail($id);
            $validator = Validator::make($request->all(),[
            'nama_kriteria' => 'required|string',
            'attribut'      => 'required|string',
            'bobot'         => 'required|numeric',
            ]);
    
            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            $kriteria->update([
                'nama_kriteria' => $request->nama_kriteria,
                'attribut' => $request->attribut,
                'bobot' => $request->bobot
            ]);

            DB::commit();
                return back()->with('success', 'Kriteria berhasil diedit!');
            }catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
            }
    }

    public function destroy($id)
    {
        // $kriteria = Kriteria::findOrFail($id);
        // $kriteria->delete();
        // return redirect()->route('kriteria.index')->with('success', 'Data berhasil dihapus.');
        try{
            DB::beginTransaction();
            $kriteria = Kriteria::findOrFail($id);
            $kriteria->delete();
            DB::commit();
                return back()->with('success', 'Kriteria berhasil dihapus!');
        }   catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
        }
    }

    public function show($id)
    {
        $data['crips'] = Crips::where('kriteria_id', $id)->get();
        $data['kriteria'] = Kriteria::findOrFail($id);
        return view('admin.kriteria.show', $data);
    }
}
