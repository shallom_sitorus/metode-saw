<?php

namespace App\Http\Controllers;

use App\Models\Alternatif;
use App\Models\Crips;
use App\Models\Kriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CripsController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    // public function index()
    // {
    //     $data['alternatif'] = Alternatif::orderBy('nama_alternatif', 'ASC')->get();
    //     return view('admin.alternatif.index', $data);
    // }

    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $validator = Validator::make($request->all(),[
                'nama_crips' => 'required|string',
                'bobot' => 'required|numeric',
            ]);
    
            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
    
            Crips::create([
                'kriteria_id' => request()->kriteria_id,
                'nama_crips' => request()->nama_crips,
                'bobot' => request()->bobot,
            ]);

            DB::commit();
            return back()->with('success', 'Crips berhasil ditambahkan!');
        }catch(\Exception $e){
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
        
    }
    public function edit($id)
    {
        $data['crips'] = Crips::findOrFail($id);

        return view('admin.crips.edit', $data);
    }

    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $crips = Crips::findOrFail($id);
            $validator = Validator::make($request->all(),[
                'nama_crips' => 'required|string',
                'bobot' => 'required|numeric',
            ]);
    
            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            $crips->update([
                'nama_crips' => request()->nama_crips,
                'bobot' => request()->bobot,
            ]);

            DB::commit();
                return back()->with('success', 'Crips berhasil diedit!');
            }catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
            }
    }

    public function destroy($id)
    {
        try{
            $crips = Crips::findOrFail($id);
            $crips->delete();
            DB::commit();
                return back()->with('success', 'Crips berhasil dihapus!');
        }   catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
        }
    }
}
