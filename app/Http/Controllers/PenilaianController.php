<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use App\Models\Alternatif;
use App\Models\Penilaian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PenilaianController extends Controller
{
    public function index()
    {
        $alternatif = Alternatif::with('penilaian.crips')->get();
        // return response()->json($alternatif);
        $kriteria = Kriteria::with('crips')->orderBy('nama_kriteria', 'ASC')->get();
        return view('admin.penilaian.index',compact('alternatif', 'kriteria'));
    }

    public function store(Request $request)
    {
        // dd($request);
        // return response()->json($request);
        // $alternatif = count($request->crips_id);
        // dd($alternatif);
        // for ($i=0; $i < $alternatif ; $i++) { 
            
        // }
        try{
            DB::select("TRUNCATE penilaian");
            DB::beginTransaction();
            foreach ($request->crips_id as $key => $value) {
                foreach($value as $key_1 => $value_1)
                {
                    Penilaian::create([
                        'alternatif_id' => $key,
                        'crips_id'      => $value_1
                    ]);
                }
            }
            DB::commit();
                return back()->with('success', 'Penilaian berhasil ditambahkan!');
            }catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
            }
       
    }
}
