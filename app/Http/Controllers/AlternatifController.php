<?php

namespace App\Http\Controllers;

use App\Models\Alternatif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AlternatifController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['alternatif'] = Alternatif::orderBy('nama_alternatif', 'ASC')->get();
        return view('admin.alternatif.index', $data);
    }

    public function store(Request $request)
    {
        // dd($request);
        try{
            DB::beginTransaction();
            $validator = Validator::make($request->all(),[
                'nama_alternatif' => 'required|string',
            ]);
    
            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
    
            Alternatif::create([
                'nama_alternatif' => request()->nama_alternatif,
            ]);

            DB::commit();
            return redirect('alternatif')->with('success', 'Alternatif berhasil ditambahkan!');
        }catch(\Exception $e){
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
        
    }
    public function edit($id)
    {
        $data['alternatif'] = Alternatif::findOrFail($id);
        return view('admin.alternatif.edit', $data);
    }

    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $alternatif = Alternatif::findOrFail($id);
            $validator = Validator::make($request->all(),[
            'nama_alternatif' => 'required|string',
            ]);
    
            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            $alternatif->update([
                'nama_alternatif' => $request->nama_alternatif,
            ]);

            DB::commit();
                return back()->with('success', 'Alternatif berhasil diedit!');
            }catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
            }
    }

    public function destroy($id)
    {
        // $kriteria = Alternatif::findOrFail($id);
        // $kriteria->delete();
        // return redirect()->route('kriteria.index')->with('success', 'Data berhasil dihapus.');
        try{
            $alternatif = Alternatif::findOrFail($id);
            $alternatif->delete();
            DB::commit();
                return back()->with('success', 'Alternatif berhasil dihapus!');
        }   catch(\Exception $e){
                DB::rollback(); 
                return back()->with('warning','Something Went Wrong!');
        }
    }
}
