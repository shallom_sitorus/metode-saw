<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\AlternatifController;
use App\Http\Controllers\AlgoritmaController;
use App\Http\Controllers\CripsController;
use App\Http\Controllers\PenilaianController;
use App\Models\Kriteria;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('kriteria', KriteriaController::class)->except(['create']);
Route::resource('alternatif', AlternatifController::class)->except(['create', 'show']);
Route::resource('crips', CripsController::class)->except(['index', 'create', 'show']);
Route::resource('/penilaian', PenilaianController::class);
// Route::resource('penilaian', 'App\Http\Controllers\PenilaianController');
Route::get('/perhitungan', [AlgoritmaController::class, 'index'])->name('perhitungan.index');
// Route::resource('/perhitungan', 'App\Http\Controllers\AlgoritmaController');
// Route::delete('/kriteria/{id}', 'KriteriaController@destroy');
// Route::get('/kriteria', [KriteriaController::class, 'index']);
// Route::post('/kriteria/store', [KriteriaController::class, 'store']);
// Route::get('/kriteria/edit/{id}', [KriteriaController::class, 'edit']);
// Route::put('/kriteria/update/{id}', [KriteriaController::class, 'update']);
// Route::post('/kriteria/delete/{id}', [KriteriaController::class, 'destroy']);
// Route::get('/','HomeController@index')->name('home');
// Route::get('/kriteria', [App\Http\Controllers\KriteriaController::class, 'index']);
// Route::resource("kriteria", "KriteriaController");
// Route::middleware(['auth'])->controller(KriteriaController::class)->group(function () {
//     Route::resource('kriteria');
// });
